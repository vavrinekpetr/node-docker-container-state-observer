## Docker Container Observer

This project provides a script to monitor the state of specified Docker containers and execute user-defined scripts based on their exit status.

### Installation

1. **Dependencies:** Ensure you have Node.js and `npm` installed.
2. **Clone the repository:** 
```
git clone https://your-repository-url.git
```
3. **Install dependencies:**
```
cd docker-container-observer
npm install
```

### Usage

```
node index.js --container <container_name_or_id> [--on-exit <command>] [--on-not-found <command>] [--mode <mode>] [--interval <interval>]
```

**Arguments:**

* **--container (required):** Comma-separated list of container names or IDs to monitor.
* **--on-exit (optional):** Command to execute when all (or any) container(s) exit.
* **--on-not-found (optional):** Command to execute if a specified container is not found.
* **--mode (optional):** Check mode. Possible values: "all" (all containers must exit) or "any" (at least one container must exit). Defaults to "all".
* **--interval (optional):** Interval between checks in seconds. Defaults to 120 seconds (2 minutes).


**Example Usage:**

```
node index.js --container my-container --on-exit "node restart-script.js" --mode any
```

This command will monitor the container named "my-container" and execute the script "restart-script.js" if the container exits.

### Functionality

* The script parses command-line arguments.
* It utilizes the `args-parser` module to handle argument parsing.
* It uses the `winston` library for logging with timestamps and colorization (optional).
* The `child_process` module is used to execute external commands.
* The script retrieves the state of specified container(s) using the `docker ps` command and parses the output using `JSON.parse`.
* Based on the specified mode ("all" or "any") and container exit state, the script decides whether to call the exit or not-found command.
* The script runs checks periodically based on the specified interval.


## Building and Distributing

This project also provides a way to create a standalone binary executable using the `pkg` module. This can be useful for deploying the script without requiring Node.js on the target system.

**Building the Binary:**

1. Ensure you have `pkg` installed. You can install it globally:

```
npm install -g pkg
```

or locally:

```
npm install pkg
```

2. Navigate to the project directory.

3. Run one of the following commands:

* **With `pkg` installed globally:**

```
npm run build
```

* **With `pkg` installed locally:**

```
npx pkg . --output build/docker-observer
```

This will create a binary executable named `docker-observer` in the `build_exec` directory.

**Packaging for Distribution:**

The `.gitlab-ci.yml` file included in this project demonstrates how to automatically build and package the binary executable during the CI/CD pipeline on GitLab. This is optional, but it can streamline the distribution process for your project.

**Using the Binary:**

The binary executable can be used similarly to the Node.js script:

```
./docker-observer --container <container_name_or_id> [--on-exit <command>] [--on-not-found <command>] [--mode <mode>] [--interval <interval>]
```

**Note:** Building the binary requires Node.js to be installed on the system where the build is performed.

### License

This project is licensed under the MIT License.

### Contact

For any questions or issues, please contact the project maintainer.

