import winston from 'winston';


const myFormat = winston.format.printf(({ level, message, timestamp }) => {
    return `${timestamp} ${level}: ${message}`;
});


export const Log = winston.createLogger({
    transports: [new winston.transports.Console({
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp(),
            myFormat
        )
    })]
});
