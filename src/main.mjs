import argsParser from 'args-parser';
import child_process from 'child_process';
import { Log } from './log.mjs';
import { getDockerStates, DockerState } from './docker.mjs';

const args = argsParser(process.argv);

// Arguments
const containerArg = args["container"];
const onExitArg = args["on-exit"];
const onNotFoundArg = args["on-not-found"];
const modeArg = args["mode"] ?? "all";
let INTERVAL = args["interval"] || 2 * 60

if (!containerArg || typeof containerArg !== "string") {
  throw new Error("container argument not valid");
}

const observeContainerIds = containerArg.split(",");

if (!containerArg || !observeContainerIds) {
  Log.error("Invalid container name at $0");
  process.exit(1);
}

if (isNaN(INTERVAL)) {
  Log.error("Entered interval is not numeric type");
  process.exit(1);
} else {
  INTERVAL = Number(INTERVAL);
}

/**
 * all - all containers must exit
 * any - at least one container must exit
 */
if (!["all", "any"].includes(modeArg)) {
  Log.error("Only available options for mode are: all, any");
  process.exit(1);
}


Log.info(`Started observing for: ${observeContainerIds.join(',')}`);
Log.info(`Check interval set to: ${INTERVAL} s`);
Log.info(`Check mode: ${modeArg}`);

const handleOut = (err, data) => {
  if (err) {
    Log.error(data);
    process.exit(1);
  }
  Log.info(data);
};


const check = async () => {
  Log.info("Performing check...");

  const onNotFound = (containerId) => {
    clearInterval(intervalHandler);
    Log.info(`Container(s) ${containerId} could not be found`);
    if (onNotFoundArg)
      child_process.exec(onNotFoundArg, handleOut);
  }

  const onExit = (containerId) => {
    clearInterval(intervalHandler);
    Log.info(`Container(s) (${containerId}) exited`);

    if (onExitArg)
      child_process.exec(onExitArg, handleOut);
  }

  const result = await getDockerStates(observeContainerIds);

  let nRunning = 0;
  for (const containerId of observeContainerIds) {
    if (!(containerId in result)) {
      if (modeArg == "any") {
        onNotFound(containerId);
        return;
      }
    }

    const state = result[containerId];

    if (state == DockerState.running)
      nRunning++;


    if (modeArg == "any" && state != DockerState.running) {
      onExit(containerId);
      return;
    }
  }

  // Still running
  if (nRunning > 0) {
    Log.info(`${nRunning} container(s) still running`);
    return;
  }

  onExit("all");
}

let intervalHandler = setInterval(check, INTERVAL * 1000);
