import { promisify } from 'util';
import child_process from 'child_process';

const execAsync = promisify(child_process.exec);


export const DockerState = {
    created: "created",
    running: "running",
    paused: "paused",
    exited: "exited"
}

/**
 * 
 * @param {string | string[]} containerIds Array of container ids
 * @returns {Record<string, keyof DockerState>}
 * 
 * {
 *  [containerId]: "Running" <--- status
 * }
 */
export const getDockerStates = async (containerIds) => {
    const { stderr, stdout } = await execAsync("docker ps --format='{{json .}}' -a");

    if (stdout.trim().length == 0) {
        return {};
    }

    let contents = stdout.replaceAll('\n', ',');
    contents = contents.substring(0, contents.lastIndexOf(','));
    const data = JSON.parse(`[${contents}]`);
    return containerIds.reduce((prev, current) => {

        const container = data.find(e => current == e.Names);

        if (container)
            return { ...prev, [current]: container.State };

        return prev
    }, {});
}

/**
 * 
 * @param {string} containerId 
 * @returns {keoyf DockerState} State of docker container
 */
export const getDockerState = async (containerId) => {
    const result = await getDockerStates([containerId]);
    return result[containerId] ?? null;
}